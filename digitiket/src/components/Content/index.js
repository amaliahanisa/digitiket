import React from 'react';
import './style.css';
import picture1 from  '../../images/azhar-galih-IFcmpsiuY3k-unsplash 1.png';
import picture2 from  '../../images/kimo-475452-unsplash 1.png';
import picture3 from  '../../images/kimo-475452-unsplash 2.png';
import picture4 from  '../../images/kimo-475452-unsplash 3.png';
import infologo from  '../../images/information-circle-outline 1.png';
import peta from  '../../images/peta.png';
import logoinstagram from '../../images/logo-instagram.png';
import logowhatsapp from '../../images/logo-whatsapp.png';
import logotwitter from '../../images/logo-twitter.png';
import logoyoutube from '../../images/logo-youtube.png';
import logoplaystore from '../../images/1200px-Google_Play_Store_badge_EN 1.png';
import image1 from '../../images/image 1.png';
import image2 from '../../images/image 2.png';
import logominus from '../../images/add 1.png';
import logoplus from '../../images/add 2.png';


function Content() {
  return (
      <div>
          <div class="content1">
        <div class="column1">
            <img src={picture1} alt="pic1"></img>
        </div >
        <div class="column2">
            <img src={picture2} alt="pic2"></img>
            <img src={picture3} alt="pic3"></img>
            <img src={picture4} alt="pic4"></img>
        </div>
        <div class="column3">
            <table rules="none">
                <tr>
                    <th colspan="3">Jam Buka</th>
                </tr>
                <tr>
                    <td>Sunday</td>
                    <td>10:00 - 17.00</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Sunday</td>
                    <td>10:00 - 17.00</td>
                    <td><span class="purple-big">Today</span></td>
                </tr>
                <tr>
                    <td>Sunday</td>
                    <td>10:00 - 17.00</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Sunday</td>
                    <td>10:00 - 17.00</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Sunday</td>
                    <td>10:00 - 17.00</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Sunday</td>
                    <td>10:00 - 17.00</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Sunday</td>
                    <td>10:00 - 17.00</td>
                    <td></td>
                </tr>
                <tr>
                    <th colspan="3">Informasi Pengguna</th>
                </tr>
                <tr class="no-border">
                    <td >Masa Aktif</td>
                    <td>1 Hari</td>
                    <td></td>
                </tr>
                <tr class="no-border">
                    <td>Harga</td>
                    <td>IDR 50.000</td>
                    <td><span class="purple-small">allday</span></td>
                </tr>
                <tr class="no-border">
                    <td>Sunday</td>
                    <td>IDR 50.000</td>
                    <td><span class="pink">weekend</span></td>
                </tr>
            </table>
        </div>
    </div>

    <div class="content-info">
        <img src={infologo} alt="infologo"></img>
        <p>Free wahana: Kora-kora, Mini Train, Flying Elephant</p>
    </div>

    <div class="content-desc">
        <div class="desc1">
            <p class="content-title">Virtual Tour Semeru</p>
            <div class="line-border"></div>
            <p class="paragraph">Fasilitas yang di tawarkan di Water Splash Darmawangsa pengunjung bisa menjadi member dan keuntungannya setiap pembelian tiket masuk mendapatkan potongan harga sebesar 2000 sampai 3000 rupiah dari harga tiket masuk normal. Water Splash Darmawangsa Residence menyediakan 3 kolam renang diantaranya, 1 kolam renang untuk dewasa atau remaja, dan yang 2 kolam renang khusus untuk anak-anak, tersedia pula seluncuran air yang di katagorikan untuk anak-anak karena memiliki ketinggian yang cukup rendah.</p>
            <div class="logocontainer">
                <div><p class="share">Share</p></div>
                <div><img src={logoinstagram} alt="infologo"></img></div>
                <div><img src={logotwitter} alt="infologo"></img></div>
                <div><img src={logowhatsapp} alt="infologo"></img></div>
                <div><img src={logoyoutube} alt="infologo"></img></div>
            </div>
        
            <p class="content-title">About</p>
            <div class="line-border"></div>
            <p class="content-title-small">Alamat</p>
            <p class="paragraph">
            Darmawangsa
            Jl. Darmawangsa Raya, Satriajaya, Tambun Utara, Bekasi, Jawa Barat 17510
            , Bekasi
            , Jawa Barat</p>

            <p class="content-title">Tiket Watersplash</p>
            <div class="line-border"></div>
            <p class="content-title-small">Alamat</p>
            <p class="paragraph">
            Darmawangsa
            Jl. Darmawangsa Raya, Satriajaya, Tambun Utara, Bekasi, Jawa Barat 17510
            , Bekasi
            , Jawa Barat</p>

            <p class="content-title">Peta Lokasi</p>
            <div class="line-border"></div>
            <img src={peta} alt="peta"></img>
        
        </div>
        <div class="desc2">
            <p class="content-title">Pilih Tiket</p>
            <div class="options">
                <div class="option1">
                    <p class="option-text">Tentukan Tanggal</p>
                </div>
                <div class="option2">
                    <p class="option-text">Pilih Tiket</p>
                </div>
            </div>
                <div class="tiket-div">
                    <p class="tiket-title">Tiket Dewasa</p>
                    <div class="tiket-line"></div>
                    <div class="tanggal">
                        <div class="tanggal1">
                            <p class="tanggal-text">09 Sep 2020, 06:00-20:30 WIB</p>
                        </div>
                        <div class="tanggal2">
                            <p class="tanggal-text">09 Sep 2020, 06:00-20:30 WIB</p>
                        </div>
                    </div>
                    <p class="detail-text">Detail Package</p>
                    <div class="tiket-line"></div>
                    <div class="harga"><p>IDR 40.000</p></div>
                    <div class="button">
                        <button class="button1">
                             <img src={logominus} alt="logominus"></img>
                        </button>
                        <input class="input" placeholder="2">
                        </input>
                        <button class="button2">
                        <img src={logoplus} alt="logoplus"></img>
                        </button>
                    </div>
                 </div>
                 <div class="tiket-div">
                    <p class="tiket-title">Tiket Anak-Anak (3 - 7 Tahun)</p>
                    <div class="tiket-line"></div>
                    <div class="tanggal">
                        <div class="tanggal1">
                            <p class="tanggal-text">09 Sep 2020, 06:00-20:30 WIB</p>
                        </div>
                        <div class="tanggal2">
                            <p class="tanggal-text">09 Sep 2020, 06:00-20:30 WIB</p>
                        </div>
                    </div>
                    <p class="detail-text">Detail Package</p>
                    <div class="tiket-line"></div>
                    <div class="harga"><p>IDR 30.000</p></div>
                    <div class="button">
                        <button class="button1">
                             <img src={logominus} alt="logominus"></img>
                        </button>
                        <input class="input" placeholder="2">
                        </input>
                        <button class="button2">
                        <img src={logoplus} alt="logoplus"></img>
                        </button>
                    </div>
                 </div>

                 <div class="tiket-div">
                    <p class="tiket-title">Tiket Bayi (0 - 2 Tahun)</p>
                    <div class="tiket-line"></div>
                    <div class="tanggal">
                        <div class="tanggal1">
                            <p class="tanggal-text">09 Sep 2020, 06:00-20:30 WIB</p>
                        </div>
                        <div class="tanggal2">
                            <p class="tanggal-text">09 Sep 2020, 06:00-20:30 WIB</p>
                        </div>
                    </div>
                    <p class="detail-text">Detail Package</p>
                    <div class="tiket-line"></div>
                    <div class="harga"><p>IDR 0.000</p></div>
                    <div class="button">
                        <button class="button1">
                             <img src={logominus} alt="logominus"></img>
                        </button>
                        <input class="input" placeholder="0">
                        </input>
                        <button class="button2">
                        <img src={logoplus} alt="logoplus"></img>
                        </button>
                    </div>
                 </div>
                 <div class="bottom">
                <div class="total">
                    <p class="total-text">Total            IDR 140.000</p>
                </div>
                <button class="checkout">
                    <p class="checkout-text">Checkout</p>
                </button>
            </div>
        </div>
        
    </div>

        <div class="footer">
            <div class="footer1">
                <p class="footer-title">Langganan Newsletter</p>
                <p class="footer-p">Jadi orang yang pertama tahu info & promosi
                        Wisata, Event, Terbaru. Gratis.</p>

                <p class="footer-title">Download DigiTiket Apps</p>
                <img src={logoplaystore} alt="logoplaystore"></img>
            </div>
            <div class="footer2">
                <p class="footer-title">Perusahaan</p>
                <p class="footer-p">Karir</p>
                <p class="footer-p">Blog</p>
                <p class="footer-p">Jadi Partner DigiTiket</p>
                <p class="footer-p">DigiTiket Press Room</p>
                <p class="footer-p">Brand Guidelines</p>
                <p class="footer-p">Alamat Kantor</p>
            </div>
            <div class="footer3">
                 <p class="footer-title">Bantuan</p>
                 <p class="footer-p">Cara Pesan</p>
                <p class="footer-p">FAQ</p>
                <p class="footer-p">Kebijakan</p>
                <p class="footer-p">Kebijakan dan Privasi</p>
                <p class="footer-p">Syarat dan Ketentuan</p>
            </div>
            <div class="footer4">
                <p class="footer-title">Berkolaborasi</p>
                <img src={image1} alt="logoplaystore"></img>
                <p class="footer-title">Metode Pembayaran</p>
                <img src={image2} alt="logoplaystore"></img>
            </div>
        </div>
      </div>
    
  );
}

export default Content;
