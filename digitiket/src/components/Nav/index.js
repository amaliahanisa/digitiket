import React from 'react';
import './style.css';
import Searchbar from '../Searchbar/index';
import digitiketogo from '../../images/digitiketlogo.png';
import attractionlogo from '../../images/attractionlogo.png';
import eventlogo from '../../images/eventlogo.png';
import virtualtourlogo from '../../images/virtualtourlogo.png';
import locationlogo from '../../images/locationlogo.png';
import arrow from '../../images/arrow.png';
import plus from '../../images/plus.png';

function Nav() {
  return (
    <nav class="navbar">
        <img class="digitiketlogo" src={digitiketogo} alt="digitiketlogo" ></img>
        <Searchbar />
        <button class="loginbutton">Login</button>
        <div class="line1"/>
        <div class="menu">
            <div class="attraction-div">
                <img class="attraction-img" src={attractionlogo} alt="attractionlogo"></img>
                <span class="title">Attraction</span>
            </div>
            <div class="event-div">
                <img src={eventlogo} alt="eventlogo"></img>
                <span class="title">Event</span>
            </div>
            <div class="virtualtour-div">
                <img src={virtualtourlogo} alt="virtualtourlogo"></img>
                <span class="title">Virtual Tour</span>
                <span class="new">New</span>
            </div>
            <div class="location-div">
                <img src={locationlogo} alt="locationlogo"></img>
                <span class="location-text">Semarang</span>
            </div>
            <div class="buattiket-div">
                <img class="pluslogo" src={plus} alt="plus"></img>
               <span>Buat Tiket</span>
            </div>
        </div>
    </nav>
  );
}

export default Nav;