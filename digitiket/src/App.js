import './App.css';
import Nav from './components/Nav/index';
import Content from './components/Content/index';

function App() {
  return (
    <div className="App">
      <Nav />
      <Content />
    </div>
  );
}

export default App;
